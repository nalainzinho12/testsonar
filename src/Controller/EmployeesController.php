<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Utility\Text;
use Cake\Routing\Router;
use Cake\Utility\Xml;

/**
 * Employees Controller
 *
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $employees = $this->paginate($this->Employees);

        $this->set(compact('employees'));
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('employee'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $employee = $this->Employees->newEmptyEntity();

        if ($this->request->is('post')) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
        }

        $this->set(compact($employee));
    }

    /**
     * Edit method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => [],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $employee = $this->Employees->patchEntity($employee, $this->request->getData());
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The employee could not be saved. Please, try again.'));
        }

        $this->set(compact('employee'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // $this->request->allowMethod(['post', 'delete']);
        
        $employee = $this->Employees->get($id);

        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('The employee has been deleted.'));
        } else {
            $this->Flash->error(__('The employee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function formatXml()
    {
        $xmlArray = [
            'liste' => [
                'Personne' =>[
                    [
                        'nom' => 'NAMBININTSOA',
                        'prenom' => 'Alain zinho',
                        'adresse' => 'Lot II M 8 ter'
                    ],
                    [ 
                        'nom' => 'BLAISE',
                        'prenom' => 'Rayan',
                        'adresse' => 'Lot II M 8 ter'
                    ]
                ]
            ]
        ];
        $xmlObject = Xml::fromArray($xmlArray);
        $xmlString = $xmlObject->asXML();

        $dom = new \DOMDocument();
        $dom->formatOutput = true;
	  
        $dirForm = new Folder( WWW_ROOT . 'import'. DS .'formated'. DS, true, 0755);

        if(file_exists($dirForm->pwd().DS.'content.xml')){
            unlink($dirForm->pwd().DS.'content.xml');
        }
        $xmlFile = new File($dirForm->pwd().DS.'content.xml', true, 0755);
        $file = $xmlFile->path;
        $isXml = $xmlFile->write($xmlString);

        if($isXml) {
	        $format = $dom->load($file);
            $dom->save($file);
        }
    }
}
