<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * XmlFormat Controller
 *
 * @method \App\Model\Entity\XmlFormat[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class XmlFormatController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $xmlFormat = $this->paginate($this->XmlFormat);

        $this->set(compact('xmlFormat'));
    }

    /**
     * View method
     *
     * @param string|null $id Xml Format id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $xmlFormat = $this->XmlFormat->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('xmlFormat'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $xmlFormat = $this->XmlFormat->newEmptyEntity();
        if ($this->request->is('post')) {
            $xmlFormat = $this->XmlFormat->patchEntity($xmlFormat, $this->request->getData());
            if ($this->XmlFormat->save($xmlFormat)) {
                $this->Flash->success(__('The xml format has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The xml format could not be saved. Please, try again.'));
        }
        $this->set(compact('xmlFormat'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Xml Format id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $xmlFormat = $this->XmlFormat->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $xmlFormat = $this->XmlFormat->patchEntity($xmlFormat, $this->request->getData());
            if ($this->XmlFormat->save($xmlFormat)) {
                $this->Flash->success(__('The xml format has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The xml format could not be saved. Please, try again.'));
        }
        $this->set(compact('xmlFormat'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Xml Format id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $xmlFormat = $this->XmlFormat->get($id);
        if ($this->XmlFormat->delete($xmlFormat)) {
            $this->Flash->success(__('The xml format has been deleted.'));
        } else {
            $this->Flash->error(__('The xml format could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
