<h1>Ajout employee</h1>

<?php 
    echo $this->Form->create();
    echo $this->Form->control('id', ['type' => 'hidden']);
    echo $this->Form->control('nom');
    echo $this->Form->control('prenom');
    echo $this->Form->control('immatricule');
    echo $this->Form->control('email');
    echo $this->Form->control('fonction');
    echo $this->Form->control('departement');
    echo $this->Form->button('Enregistrer');
    echo $this->Form->end();
?>