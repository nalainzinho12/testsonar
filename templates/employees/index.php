<h1 id="title">Liste des employées</h1>
<button class="btn btn-success"><?= $this->Html->link('Add employee', ['action' => 'add']) ?></button>

<table>
    <thead>
        <tr>
            <th id="">Nom</th>
            <th id="">Prenom</th>
            <th id="">Immatricule</th>
            <th id="">Email</th>
            <th id="">Fonction</th>
            <th id="">Département</th>
            <th id="">Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($employees as $employee) : ?>
            <tr>
                <td>
                    <?= $employee->nom ?>
                </td>
                <td>
                    <?= $employee->prenom ?>
                </td>
                <td>
                    <?= $employee->immatricule ?>
                </td>
                <td>
                    <?= $employee->email ?>
                </td>
                <td>
                    <?= $employee->fonction ?>
                </td>
                <td>
                    <?= $employee->departement ?>
                </td>
                <td>
                    <!-- link( affichage, lien diriger lors du click) -->
                    <?= $this->html->link('View', ['action' => 'view', $employee->id]) ?>
                </td>
                <td>
                    <?= $this->html->link('Edit', ['action' => 'edit', $employee->id]) ?>
                </td>
                <td>
                    <?= $this->html->link('Delete', ['action' => 'delete', $employee->id], ['confirm' => 'Are you sure?']) ?>
                </td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>